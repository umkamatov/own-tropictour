// store/index.js
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        modal: {
            isOpen: false,
            title: "Оставить заявку",
            url: "/form-main",
            desc: "Заполните поля ниже и оставьте заявку, наши специалисты свяжутся с вами чтобы обсудить дальнейшие действия",
            form: "Главная форма обратной связи",
        },
    },
    mutations: {
        MODAL_SET_DATA(state, payload) {
            if (payload) {
                Object.keys(payload).forEach((element) => {
                    state.modal[element] = payload[element];
                });
            }
            state.modal.isOpen = true;
        },
        MODAL_CLOSE(state) {
            state.modal.isOpen = false;
        },
    },
    actions: {
        openModal({commit}, payload) {
            commit("MODAL_SET_DATA", payload);
        },
        closeModal({commit}) {
            commit("MODAL_CLOSE");
        },
    },
    getters: {
        modalParams: (state) => state.modal,
    },
});

export default store;
